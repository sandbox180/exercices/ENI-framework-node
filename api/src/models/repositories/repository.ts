import {Entity} from "../types/entity";

export interface Repository<T extends Entity> {
  getAll(): Promise<T[]>;
  insert(entity : T): Promise<void>;
}