import {Entity} from "./entity";

export interface Employee extends Entity {
  firstName: string;
  lastName: string;
  email: string;
  teamId: number;
}