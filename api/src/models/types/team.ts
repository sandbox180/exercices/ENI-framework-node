import {Entity} from "./entity";

export interface Team extends Entity {
  id: number
  name: string
}


