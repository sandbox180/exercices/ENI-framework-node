import "reflect-metadata";
import { config } from "dotenv";
import express from "express";
import morgan from "morgan";
import cors from "cors";
import bodyParser from "body-parser";
import {routeCollection} from "./infrastructure/routeCollection";


/*----------- Decorators Executions --------- */
import "./models/repositories/employeeRepository";
import "./controllers/employeeController";
import "./controllers/teamController";
import "./models/repositories/teamRepository";


/*----------- App Initialisation  --------- */
config()
const app = express()
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(morgan("dev"));
app.use(cors());


const port = process.env.PORT || 3001;
const router = express.Router();

app.get("/", (req,res) => res.send("Hello World!"));
routeCollection.setupRouter(router);
app.use(router);

app.listen(
  port, () =>
    console.log(`Application is listening on port ${port}!`)
);
