import {Controller} from "../decorators/controller";
import {TeamRepository} from "../models/repositories/teamRepository";
import {Inject} from "../decorators/inject";
import {Keys} from "../ keys";
import {Get} from "../decorators/route";
import {Request, Response} from "express";

@Controller()
class TeamController {
  private readonly teamRepository: TeamRepository;

  constructor(
    @Inject(Keys.teamRepository) teamRepository: TeamRepository
  ) {
    this.teamRepository = teamRepository;
  }

  @Get()
  async getAll(request: Request, response: Response) {
    const teams = await this.teamRepository.getAll();
    response.json(teams);
  }

}
