import {dependencyService} from "../infrastructure/dependencyService";

export const Dependency = (key: Symbol) => {
  const decorator: ClassDecorator = (target: any) => {
    dependencyService.add(key, target);
  };

  return decorator;
};