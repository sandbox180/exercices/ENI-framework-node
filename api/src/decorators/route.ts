import { HttpVerb, routeCollection } from "../infrastructure/routeCollection";

//Application Partielle
const action = (httpVerb: HttpVerb, path?: string) => {
  const decorator: MethodDecorator = (
    target: any,
    key: string | Symbol,
    descriptor: PropertyDescriptor
  ) => {
    routeCollection.registerAction(
      target.constructor.name,
      key.toString(),
      httpVerb,
      path
    )
  }
  return decorator;
}

//Decorator Factory "GET"
const Get = (path?: string) => action('get',path)
const Post = (path?: string) => action('post',path)
const Patch = (path?: string) => action('patch',path)

export { Get,Post, Patch };